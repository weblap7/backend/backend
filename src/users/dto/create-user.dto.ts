import { IsEmail, IsNotEmpty, Length } from 'class-validator';
export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(10, 20) // (min,max)
  password: string;

  @IsNotEmpty()
  @Length(4, 32)
  fullname: string;

  // @IsArray()
  // roles: ('admin' | 'user')[];

  @IsNotEmpty()
  gender: 'male' | 'female' | 'other';
}
